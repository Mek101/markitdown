# Why no Navigation Component?

The file manager has 2 possible views:

- The directory with the files, that can be stacked on one another.

- The "permission missing" view.

We need to be able to dynamically switch those 2 possible views as the starting view, and if the file storage directory changes, we need to clean the stack and restart from a new root.

The access to the directory may be revoked while the app is running. If this happens we need to clean the stack and display the "permission missing" view.