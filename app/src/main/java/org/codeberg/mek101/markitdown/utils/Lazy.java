package org.codeberg.mek101.markitdown.utils;

import java.util.function.Supplier;

public class Lazy<T> implements Supplier<T> {

	private Supplier<T> _value;

	public Lazy(final Supplier<T> supplier) {
		this._value = () -> {
			final T value = supplier.get();
			this._value = () -> value;
			return value;
		};
	}

	@Override
	public T get() {
		return this._value.get();
	}
}
