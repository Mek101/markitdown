package org.codeberg.mek101.markitdown.ui.files.directoryview;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.codeberg.mek101.markitdown.R;
import org.codeberg.mek101.markitdown.databinding.FragmentDirectoryViewBinding;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedDirectory;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedItem;
import org.codeberg.mek101.markitdown.viewmodel.files.directoryview.DirectoryViewModel;
import org.codeberg.mek101.markitdown.viewmodel.files.ViewModelProviderFactory;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass. Use the {@link DirectoryViewFragment#newInstance} factory method to create an
 * instance of this fragment.
 */
public class DirectoryViewFragment extends Fragment {

	private static final String DIRECTORY_URI = "folder_uri";

	private final ListAdapter<CachedItem, DirectoryViewAdapter.CachedViewItemHolder> _adapter;
	private RecyclerView _recyclerView;
	private DirectoryViewModel _model;
	private Uri _directoryUri;


	/**
	 * Use this factory method to create a new instance of this fragment using the provided parameter.
	 *
	 * @param directoryUri the URI to the folder to browse.
	 *
	 * @return A new instance of fragment FolderViewFragment.
	 */
	public static DirectoryViewFragment newInstance(@NonNull final Uri directoryUri) {
		final DirectoryViewFragment fragment = new DirectoryViewFragment();
		final Bundle args = new Bundle(1);
		args.putString(DIRECTORY_URI, directoryUri.toString());
		fragment.setArguments(args);
		return fragment;
	}

	/**
	 * Get the appropriate view model from the uri.
	 * Wrapped up in this method since it's indecent.
	 */
	private DirectoryViewModel viewModelFromUri(final Uri uri) {
		final CachedDirectory directory = new CachedDirectory(DocumentFile.fromTreeUri(getContext(), uri));
		final ViewModelProvider viewModelProvider = ViewModelProviderFactory.getDirectoryViewModelProvider(this, getActivity().getApplication(), directory);
		return viewModelProvider.get(uri.toString(), DirectoryViewModel.class);
	}

	private String getLogName() {
		return "DirectoryViewFragment[" + this._directoryUri + "]";
	}

	public DirectoryViewFragment() {
		this._adapter = new DirectoryViewAdapter();
		this._adapter.submitList(new ArrayList<>(0));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		if (getArguments() == null) {
			throw new IllegalArgumentException("Missing directory uri");
		}
		super.onCreate(savedInstanceState);
		this._directoryUri = Uri.parse(getArguments().getString(DIRECTORY_URI));
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		this._model = viewModelFromUri(this._directoryUri);
		// Bind the livedata lifecycle to the fragment's view instead of it's instance.
		this._model.getDirectoryItems().observe(getViewLifecycleOwner(), items -> {
			Log.i(getLogName(), "submitting list with " + items.size() + " items");
			this._adapter.submitList(items, this._adapter::notifyDataSetChanged);
		});
		this._model.getDirectoryUnavailableEvent().observe(getViewLifecycleOwner(), v -> {

		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		final View fragmentView = inflater.inflate(R.layout.fragment_directory_view, container, false);
		// IMPORTANT recycler view must be obtained from this view, otherwise it won't detect the adapter and layout
		// being added, since the inflated layout is not the one implicitly connected to this fragment.
		// See: https://stackoverflow.com/a/48389399/14855239
		this._recyclerView = fragmentView.findViewById(R.id.recycler_view);
		this._recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
		this._recyclerView.setAdapter(this._adapter);

		return fragmentView;
	}
}
