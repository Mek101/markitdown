package org.codeberg.mek101.markitdown.ui.utils;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.PreferenceFragmentCompat;

import org.codeberg.mek101.markitdown.BuildConfig;
import org.codeberg.mek101.markitdown.R;

public class AboutFragment extends PreferenceFragmentCompat {

	/**
	 * Set the activity title.
	 */
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getActivity().setTitle(R.string.about_fragment_title);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	/**
	 * Load the fragment's xml and set the build version number.
	 */
	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.about, rootKey);
		findPreference("build_version").setSummary(BuildConfig.VERSION_NAME);
	}
}
