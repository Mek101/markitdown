package org.codeberg.mek101.markitdown.backend.settings;

public enum LoadRemoteImages {
	ALWAYS("always"),
	WIFI("wifi"),
	NEVER("never");

	private final String _key;

	private LoadRemoteImages(final String key) {
		this._key = key;
	}

	/**
	 * Get the correct enum value from the given key.
	 */
	public static LoadRemoteImages getFromKey(final String key) {
		for (LoadRemoteImages value : LoadRemoteImages.values()) {
			if (value.getKey().equals(key)) {
				return value;
			}
		}
		throw new IllegalArgumentException("Unknown key '" + key + "'");
	}

	public String getKey() {
		return this._key;
	}
}
