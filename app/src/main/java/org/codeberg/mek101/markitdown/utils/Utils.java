package org.codeberg.mek101.markitdown.utils;

import android.app.Application;
import android.content.Context;
import android.net.Uri;

import androidx.documentfile.provider.DocumentFile;

public class Utils {

	private Utils() { }

	public static boolean isDocumentTreeAccessible(final Context context, final Uri uri) {
		final DocumentFile documentFile = DocumentFile.fromTreeUri(context, uri);
		return documentFile != null && documentFile.canRead();
	}
}
