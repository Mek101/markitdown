package org.codeberg.mek101.markitdown.viewmodel.files.directoryview;

import androidx.lifecycle.MutableLiveData;

class NotifyingMutableLiveData<T> extends MutableLiveData<T> {

	private Runnable _onActiveCallback;
	private Runnable _onInactiveCallback;

	@Override
	protected void onActive() {
		if (this._onActiveCallback != null)  {
			this._onActiveCallback.run();
		}
	}

	@Override
	protected void onInactive() {
		if (this._onInactiveCallback != null) {
			this._onInactiveCallback.run();
		}
	}

	public void setOnActiveCallback(final Runnable callback) {
		this._onActiveCallback = callback;
	}

	public void setOnInactiveCallback(final Runnable callback) {
		this._onInactiveCallback = callback;
	}
}
