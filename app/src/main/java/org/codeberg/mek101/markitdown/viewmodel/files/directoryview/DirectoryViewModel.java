package org.codeberg.mek101.markitdown.viewmodel.files.directoryview;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.codeberg.mek101.markitdown.backend.files.DirectoryObserver;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedDirectory;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedItem;
import org.codeberg.mek101.markitdown.backend.settings.SettingsStorage;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryViewModel extends AndroidViewModel {

	// 40 seconds in milliseconds.
	private static final int STOP_OBSERVER_DELAY = 40 * 1000;

	private final NotifyingMutableLiveData<List<CachedItem>> _directoryItems;
	private final NotifyingMutableLiveData<Void> _directoryUnavailable;
	private final Handler _stopObserverTimerHandler;
	// Hold a reference to those, otherwise the JVM might deallocate them.
	private final SettingsStorage _settings;
	private final DirectoryObserver _observer;

	/**
	 * Stops observing only if there are no active live data observers after a delay.
	 */
	private void stopObserving() {
		this._stopObserverTimerHandler.postDelayed(() -> {
			if (! this._directoryItems.hasActiveObservers() && ! this._directoryUnavailable.hasActiveObservers()) {
				this._observer.stopView();
			}
		}, STOP_OBSERVER_DELAY);
	}

	/**
	 * Starts observing if there are any active live data observers.
	 */
	private void startObserving() {
		if (this._directoryItems.hasActiveObservers() || this._directoryUnavailable.hasActiveObservers()) {
			this._observer.startView(true);
		}
	}

	private void updateList(final boolean dummy) {
		updateList(this._observer.getView());
	}

	private void updateList(final Collection<CachedItem> items) {
		final List<CachedItem> sortedItems = collectionToDisplayList(items);
		// Using postValue since the directory observer might call an update from an external thread.
		this._directoryItems.postValue(sortedItems);
	}

	/**
	 * Converts a collection to a displayable form, applying the settings.
	 */
	private List<CachedItem> collectionToDisplayList(final Collection<CachedItem> collection) {
		Stream<CachedItem> base = collection.stream();
		if (!this._settings.getShowHiddenFiles()) {
			// On Linux hidden files start with a dot.
			base = base.filter(c -> c.getName().charAt(0) != '.');
		}
		if (this._settings.getShowUnknownFiles()) {
			// Any kind of text.
			base = base.filter(c -> c.getMime().startsWith("text/"));
		}

		return base.sorted((o1, o2) -> {
			// First, choose the directory.
			if (o1.isDirectory() && !o2.isDirectory()) {
				return -1;
			} else if (o2.isDirectory() && !o1.isDirectory()) {
				return 1;
			}
			// Then, sort by name.
			return o1.getName().compareTo(o2.getName());
		}).collect(Collectors.toList());
	}

	@Override
	protected void onCleared() {
		this._observer.stopView();
		// Need to manually remove those, in order to remove all references to this object.
		this._settings.getShowUnknownFilesLive().removeObserver(this::updateList);
		this._settings.getShowHiddenFilesLive().removeObserver(this::updateList);
	}

	public DirectoryViewModel(final Application application, final CachedDirectory directory) {
		super(application);
		this._settings = SettingsStorage.of(getApplication());

		// Optimizing observing, deferring the start until at least one of them is activated.
		this._directoryItems = new NotifyingMutableLiveData<>();
		this._directoryItems.setOnActiveCallback(this::startObserving);
		this._directoryItems.setOnInactiveCallback(this::stopObserving);

		this._directoryUnavailable = new NotifyingMutableLiveData<>();
		this._directoryUnavailable.setOnActiveCallback(this::startObserving);
		this._directoryUnavailable.setOnInactiveCallback(this::stopObserving);

		this._stopObserverTimerHandler = new Handler(Looper.getMainLooper());

		// Setting up the directory observer.
		this._observer = new DirectoryObserver(directory);
		this._observer.setDirectoryUpdatedCallback(this::updateList);
		this._observer.setDirectoryUnavailableCallback(() -> this._directoryUnavailable.setValue(null));
		this._observer.startView(true);

		// If the settings are updated, we need to also refresh the view.
		this._settings.getShowUnknownFilesLive().observeForever(this::updateList);
		this._settings.getShowHiddenFilesLive().observeForever(this::updateList);
	}

	public String getTitle() {
		return this._observer.getDisplayName();
	}

	public LiveData<List<CachedItem>> getDirectoryItems() {
		return this._directoryItems;
	}

	public LiveData<Void> getDirectoryUnavailableEvent() {
		return this._directoryUnavailable;
	}
}
