package org.codeberg.mek101.markitdown.backend.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.preference.PreferenceManager;

import java.util.Optional;

/**
 * A wrapper around the shared preference API defining types and  default values.
 */
public class SettingsStorage {

	/**
	 * Must be public since it's used by the settings fragment.
	 */
	public static final String FILE_STORAGE_KEY = "file_storage";

	private static final String LOAD_REMOTE_IMAGES_KEY = "load_remote_images";
	private static final String SHOW_UNKNOWN_FILES_KEY = "show_unknown_files";
	private static final String SHOW_HIDDEN_FILES_KEY = "show_hidden_files";

	private static final String LOG_NAME = "SettingsStorage";

	private static final MutableLiveData<LoadRemoteImages> _loadRemoteImagesLive;
	private static final MutableLiveData<Optional<Uri>> _fileStorageLive;
	private static final MutableLiveData<Boolean> _showUnknownFilesLive;
	private static final MutableLiveData<Boolean> _showHiddenFilesLive;

	private final SharedPreferences _storage;

	static {
		_loadRemoteImagesLive = new MutableLiveData<>();
		_fileStorageLive = new MutableLiveData<>();
		_showUnknownFilesLive = new MutableLiveData<>();
		_showHiddenFilesLive = new MutableLiveData<>();
	}

	/**
	 * @param context the context required by the shared preferences.
	 */
	public static SettingsStorage of(final Context context) {
		return new SettingsStorage(context);
	}

	private SettingsStorage(Context context) {
		this._storage = PreferenceManager.getDefaultSharedPreferences(context);
		this._storage.registerOnSharedPreferenceChangeListener((sharedPreferences, key) ->  {
			// Dispatch the call to the appropriate callback.
			switch (key) {
				case LOAD_REMOTE_IMAGES_KEY:
					setValueOf(_loadRemoteImagesLive, getLoadRemoteImages(), key);
					break;
				case FILE_STORAGE_KEY:
					setValueOf(_fileStorageLive, getFileStorage(), key);
					break;
				case SHOW_UNKNOWN_FILES_KEY:
					setValueOf(_showUnknownFilesLive, getShowUnknownFiles(), key);
					break;
				case SHOW_HIDDEN_FILES_KEY:
					setValueOf(_showHiddenFilesLive, getShowHiddenFiles(), key);
					break;
			}
		});
	}

	private <T> void setValueOf(final MutableLiveData<T> channel, final T value, final String key) {
		Log.i(LOG_NAME, "'" + key + "' preference set to '" + value + "'");
		channel.postValue(value);
	}

	public LiveData<LoadRemoteImages> getLoadRemoteImagesLive() {
		return _loadRemoteImagesLive;
	}

	public LiveData<Optional<Uri>> getFileStorageLive() {
		return _fileStorageLive;
	}

	public LiveData<Boolean> getShowUnknownFilesLive() {
		return _showUnknownFilesLive;
	}

	public LiveData<Boolean> getShowHiddenFilesLive() {
		return _showHiddenFilesLive;
	}

	public LoadRemoteImages getLoadRemoteImages() {
		return LoadRemoteImages.getFromKey(this._storage.getString(LOAD_REMOTE_IMAGES_KEY, "always"));
	}

	public void setLoadRemoteImages(final LoadRemoteImages loadRemoteImages) {
		this._storage.edit().putString(LOAD_REMOTE_IMAGES_KEY, loadRemoteImages.getKey()).apply();
	}

	/**
	 * @return the uri to the file storage. Empty if it's not available.
	 */
	public Optional<Uri> getFileStorage() {
		final String preferenceString = this._storage.getString(FILE_STORAGE_KEY, null);
		if (preferenceString == null) {
			return Optional.empty();
		} else {
			return Optional.of(Uri.parse(preferenceString));
		}
	}

	public void setFileStorage(final Uri fileStoragePath) {
		this._storage.edit().putString(FILE_STORAGE_KEY, fileStoragePath.toString()).apply();
	}

	/**
	 * Deletes the file storage value from the settings.
	 */
	public void deleteFileStorage() {
		this._storage.edit().remove(FILE_STORAGE_KEY).apply();
	}

	public boolean getShowUnknownFiles() {
		return this._storage.getBoolean(SHOW_UNKNOWN_FILES_KEY, false);
	}

	public void setShowUnknownFiles(final boolean showUnknownFiles) {
		this._storage.edit().putBoolean(SHOW_UNKNOWN_FILES_KEY, showUnknownFiles).apply();
	}

	public boolean getShowHiddenFiles() {
		return this._storage.getBoolean(SHOW_HIDDEN_FILES_KEY, false);
	}

	public void setShowHiddenFiles(final boolean showHiddenFiles) {
		this._storage.edit().putBoolean(SHOW_HIDDEN_FILES_KEY, showHiddenFiles).apply();
	}
}
