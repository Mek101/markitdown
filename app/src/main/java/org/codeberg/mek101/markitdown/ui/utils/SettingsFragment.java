package org.codeberg.mek101.markitdown.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import org.codeberg.mek101.markitdown.R;
import org.codeberg.mek101.markitdown.backend.settings.SettingsStorage;
import org.codeberg.mek101.markitdown.utils.Utils;

import java.util.Optional;


public class SettingsFragment extends PreferenceFragmentCompat {

	private static final int TAKE_PERSISTENT_FLAGS = Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;

	/**
	 * Changes to the preferences in the UI are automatically commited to the shared preferences storage API, with the
	 * exception of the file storage uri.
	 */
	private final ActivityResultLauncher<Uri> _requestFileStorageAccess;
	private SettingsStorage _storage;
	private Preference _fileStoragePreference;

	public SettingsFragment() {
		// Must register early on.
		this._requestFileStorageAccess = registerForActivityResult(new ActivityResultContracts.OpenDocumentTree(), (uri) -> {
			// Might be null if the user aborts.
			if (uri != null) {
				// Obtain permanent access.
				getActivity().getContentResolver().takePersistableUriPermission(uri, TAKE_PERSISTENT_FLAGS);

				// Update the settings storage side.
				SettingsFragment.this._storage.setFileStorage(uri);

				// Update the view.
				SettingsFragment.this._fileStoragePreference.setSummary(getSummaryPathFromUri(uri));
			}
		});
	}

	/**
	 * Unreasonable code because it's unreasonably difficult to get a decent path to display.
	 */
	private String getSummaryPathFromUri(final Uri uri) {
		final String[] pathParts = uri.getLastPathSegment().split(":");

		StringBuilder builder = new StringBuilder();
		builder.append("/");
		for (int i = 1; i < pathParts.length; i++) {
			builder.append(pathParts[i]);
		}
		return builder.toString();
	}

	/**
	 * Get the string to display as summary whatever the a file storage is available or not.
	 */
	private String getSummaryPathFromStorage() {
		final Optional<Uri> res = this._storage.getFileStorage();
		return res.map(this::getSummaryPathFromUri).orElse("No storage available");
	}

	/**
	 * Set the activity title.
	 */
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getActivity().setTitle(R.string.settings_fragment_title);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public void onAttach(@NonNull Context context) {
		super.onAttach(context);
		this._storage = SettingsStorage.of(context);
	}

	@Override
	public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
		setPreferencesFromResource(R.xml.preferences, rootKey);

		// Bind the file storage setting to it's action.
		this._fileStoragePreference = findPreference(SettingsStorage.FILE_STORAGE_KEY);
		this._fileStoragePreference.setOnPreferenceClickListener((pref) -> {
			// Request access to a file storage, starting with the last (or default) setting.
			final Optional<Uri> res = this._storage.getFileStorage();
			this._requestFileStorageAccess.launch(res.orElse(null));
			return true;
		});
		// Set the initial summary.
		this._fileStoragePreference.setSummary(getSummaryPathFromStorage());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		this._storage = null;
	}
}
