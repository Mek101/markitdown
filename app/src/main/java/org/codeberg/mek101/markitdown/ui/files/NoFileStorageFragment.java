package org.codeberg.mek101.markitdown.ui.files;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import org.codeberg.mek101.markitdown.R;

public class NoFileStorageFragment extends Fragment {

	/**
	 * Use this factory method to create a new instance of this fragment.
	 *
	 * @return A new instance of fragment NoFileStorageFragment.
	 */
	public static NoFileStorageFragment newInstance() {
		return new NoFileStorageFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		return inflater.inflate(R.layout.fragment_no_file_storage, container, false);
	}
}
