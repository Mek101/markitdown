package org.codeberg.mek101.markitdown.backend.files;

import android.net.Uri;
import android.os.FileObserver;
import android.util.Log;

import org.codeberg.mek101.markitdown.backend.files.cached.CachedDirectory;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedItem;

import java.util.Collection;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Manages the view of a directory while being lifecycle aware. Automatically triggers the callbacks when the lifecycle
 * owner status changes or when the directory is updated. No guarantees are made on the order of items received by the
 * callbacks.
 *
 * @implNote the callbacks may be called from outside the main thread, but they will never concurrently overlap.
 */
public class DirectoryObserver {

	public enum DirectoryObservingStatus {
		// The directory is being observed.
		OBSERVING,
		// The status of the directory in unknown.
		NOT_OBSERVING,
		// The directory cannot be observed because it's unavailable
		UNAVAILABLE
	}

	// The mask for the FileObserver
	private static final int OBSERVER_MASK = FileObserver.MOVE_SELF | FileObserver.MOVED_FROM | FileObserver.MOVED_TO |
			FileObserver.DELETE | FileObserver.DELETE_SELF | FileObserver.CREATE;

	private final CachedDirectory _directory;
	private final FileObserver _fileObserver;

	private Consumer<Collection<CachedItem>> _directoryUpdatedCallback;
	private Runnable _directoryUnavailableCallback;
	private Set<CachedItem> _items;
	private DirectoryObservingStatus _observingStatus;

	private String getLogName() {
		return "DirectoryObserver[" + this._directory.getUri() + "] ";
	}

	private void invokeDirectoryUnavailable() {
		if (this._directoryUnavailableCallback != null) {
			this._directoryUnavailableCallback.run();
		}
	}

	private void invokeDirectoryUpdated(final Collection<CachedItem> itemList) {
		if (this._directoryUpdatedCallback != null) {
			this._directoryUpdatedCallback.accept(itemList);
		}
	}

	/**
	 * Perform a full directory scan.
	 */
	private void scanDirectory() {
		Log.i(getLogName(), "Initiating full scan");
		final Set<CachedItem> newItems = this._directory.getContent().collect(Collectors.toSet());
		if (!newItems.equals(this._items)) {
			Log.i(getLogName(), "Full scan complete: updating list");
			this._items = newItems;
			this._directoryUpdatedCallback.accept(newItems);
		} else {
			Log.i(getLogName(), "Full scan complete: nothing to change");
		}
	}

	/**
	 * Called by the file observer. Updates the internal file list.
	 */
	private synchronized void onDirectoryUpdateEvent(final int event, final String path) {
		if (this._observingStatus == DirectoryObservingStatus.OBSERVING) {
			switch (event) {
				case FileObserver.MOVE_SELF:
				case FileObserver.DELETE_SELF:
					Log.i(getLogName(), "Observed directory moved or deleted");
					this._observingStatus = DirectoryObservingStatus.UNAVAILABLE;
					stopView();
					this._directoryUnavailableCallback.run();
					return;
				case FileObserver.MOVED_FROM:
				case FileObserver.DELETE:
					Log.i(getLogName(), "'" + path + "' moved or deleted");
					this._items.removeIf(item -> item.getUri().getPath().equals(path));
					break;
				case FileObserver.MOVED_TO:
				case FileObserver.CREATE:
					Log.i(getLogName(), "'" + path + "' moved in or created");
					this._items.add(CachedItem.fromPath(path));
					break;
				/*
				 * FileObserver.ATTRIB and FileObserver.MODIFY are implicitly handled by just invoking the callback
				 * Does FileObserver.ATTRIB include permissions being revoked? Who knows.
				 */
			}
			invokeDirectoryUpdated(this._items);
		}
	}

	/**
	 * @param directory the directory to view.
	 */
	public DirectoryObserver(final CachedDirectory directory) {
		this._directory = directory;
		this._fileObserver = FileObserverFactory.buildFileObserver(directory.getUri(), OBSERVER_MASK, this::onDirectoryUpdateEvent);

		this._observingStatus = DirectoryObservingStatus.NOT_OBSERVING;

		/*
		 * The class should also be able ot test if the permission to access the folder has been revoked, but no idea
		 * how to do that.
		 */
	}

	/**
	 * @param callback invoked when the directory's content changes.
	 */
	public void setDirectoryUpdatedCallback(final Consumer<Collection<CachedItem>> callback) {
		this._directoryUpdatedCallback = callback;
	}

	/**
	 * @param callback invoked when the directory at the current path becomes unavailable. Might be caused by the
	 *                 directory being moved, deleted or the permissions to access it are revoked.
	 */
	public void setDirectoryUnavailableCallback(final Runnable callback) {
		this._directoryUnavailableCallback = callback;
	}

	/**
	 * @return the current directory content. Might not be up to date if it's not observing.
	 */
	public Collection<CachedItem> getView() {
		return this._items;
	}

	/**
	 * Begin observing the given directory, doing an initial scan and invoking the appropriate callbacks. Does nothing
	 * if it's already observing.
	 *
	 * @param notifyUnavailable if true, if the directory is unavailable, it will invoke the relative callback.
	 */
	public synchronized void startView(final boolean notifyUnavailable) {
		if (this._observingStatus == DirectoryObservingStatus.NOT_OBSERVING) {
			Log.i(getLogName(), "Start observing");
			if (this._directory.exists()) {
				this._observingStatus = DirectoryObservingStatus.OBSERVING;
				scanDirectory();
				this._fileObserver.startWatching();
			} else {
				this._observingStatus = DirectoryObservingStatus.UNAVAILABLE;
				if (notifyUnavailable) {
					invokeDirectoryUnavailable();
				}
			}
		}
	}

	/**
	 * Stop observing the given directory. Does nothing if it's not observing the directory or the directory is
	 * unavailable.
	 */
	public synchronized void stopView() {
		if (this._observingStatus == DirectoryObservingStatus.OBSERVING) {
			Log.i(getLogName(), "Stop observing");
			this._observingStatus = DirectoryObservingStatus.NOT_OBSERVING;
			this._fileObserver.stopWatching();
		}
	}

	public synchronized DirectoryObservingStatus getViewStatus() {
		return this._observingStatus;
	}

	public String getDisplayName() {
		return this._directory.getName();
	}

	public Uri getUri() {
		return this._directory.getUri();
	}
}
