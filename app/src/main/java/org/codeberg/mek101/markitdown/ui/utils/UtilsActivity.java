package org.codeberg.mek101.markitdown.ui.utils;

import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import org.codeberg.mek101.markitdown.R;

public class UtilsActivity extends AppCompatActivity {

	public static final String UTILITY_KEY = "util_type";
	public static final int SETTINGS_UTILITY = 0;
	public static final int ABOUT_UTILITY = 1;

	private Fragment getUtilFragment() {
		final Bundle args = getIntent().getExtras();
		if (args != null) {
			final int val = args.getInt(UTILITY_KEY);
			switch (val) {
				case SETTINGS_UTILITY:
					return new SettingsFragment();
				case ABOUT_UTILITY:
					return new AboutFragment();
				default:
					throw new IllegalArgumentException("Unknown utility type to display '" + val + "'");
			}
		}
		throw new IllegalArgumentException("Missing utility type to display");
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_utils);

		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.util_container, getUtilFragment())
				.commit();

		ActionBar actionBar = getSupportActionBar();
		if (actionBar != null) {
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}
}
