package org.codeberg.mek101.markitdown.backend.files.cached;

import androidx.documentfile.provider.DocumentFile;

public class CachedFile extends CachedItem {

	public CachedFile(final DocumentFile documentFile) {
		super(documentFile);
	}

	@Override
	public boolean isDirectory() {
		return false;
	}
}
