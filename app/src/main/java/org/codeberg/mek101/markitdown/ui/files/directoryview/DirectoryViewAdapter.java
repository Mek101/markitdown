package org.codeberg.mek101.markitdown.ui.files.directoryview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import org.codeberg.mek101.markitdown.R;
import org.codeberg.mek101.markitdown.backend.files.cached.CachedItem;

public class DirectoryViewAdapter extends ListAdapter<CachedItem, DirectoryViewAdapter.CachedViewItemHolder> {

	static class CachedViewItemHolder extends RecyclerView.ViewHolder {
		private final ImageView _imageView;
		private final TextView _mainTextView;
		private final TextView _secondaryTextView;

		public CachedViewItemHolder(@NonNull View itemView) {
			super(itemView);
			this._imageView = itemView.findViewById(R.id.item_image_view);
			this._mainTextView = itemView.findViewById(R.id.item_main_text_view);
			this._secondaryTextView = itemView.findViewById(R.id.item_secondary_text_view);
		}

		public void bindTo(final CachedItem data) {
			this._mainTextView.setText(data.getName());
			this._secondaryTextView.setText(data.getLastModifiedInstant().toString());
		}
	}

	static class DiffCachedItems extends DiffUtil.ItemCallback<CachedItem> {

		@Override
		public boolean areItemsTheSame(@NonNull CachedItem oldItem, @NonNull  CachedItem newItem) {
			return oldItem.getName().equals(newItem.getName());
		}

		@Override
		public boolean areContentsTheSame(@NonNull CachedItem oldItem, @NonNull CachedItem newItem) {
			return oldItem.equals(newItem);
		}
	}

	private static final DiffCachedItems DIFF_CALLBACK = new DiffCachedItems();

	public DirectoryViewAdapter() {
		super(DIFF_CALLBACK);
	}

	@NonNull
	@Override
	public CachedViewItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		final int recyclerViewItemLayout = R.layout.recycler_view_item;
		final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		final View layoutView = inflater.inflate(recyclerViewItemLayout, parent, false);
		return new CachedViewItemHolder(layoutView);
	}

	@Override
	public void onBindViewHolder(@NonNull DirectoryViewAdapter.CachedViewItemHolder holder, int position) {
		holder.bindTo(getItem(position));
	}
}
