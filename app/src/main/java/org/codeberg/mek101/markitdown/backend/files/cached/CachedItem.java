package org.codeberg.mek101.markitdown.backend.files.cached;

import android.net.Uri;
import android.webkit.MimeTypeMap;

import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;

import org.codeberg.mek101.markitdown.utils.Lazy;

import java.io.File;
import java.time.Instant;

public abstract class CachedItem {

	private Lazy<Uri> _uri;
	private Lazy<String> _name;
	private Lazy<String> _mime;

	public static CachedItem fromPath(final String path) {
		return fromDocumentFile(DocumentFile.fromFile(new File(path)));
	}

	public static CachedItem fromDocumentFile(final DocumentFile documentFile) {
		if (documentFile.isDirectory()) {
			return new CachedDirectory(documentFile);
		} else {
			return new CachedFile(documentFile);
		}
	}

	private void initLazy() {
		this._uri = new Lazy<>(this._documentFile::getUri);
		this._name = new Lazy<>(this._documentFile::getName);
		this._mime = new Lazy<>(() -> {
			String mime = this._documentFile.getType();
			// Try getting the mime from the extension
			if (mime == null) {
				final String extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(this.getUri().toString());
				if (extension != null) {
					mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
				}
			}
			return mime == null ? "" : mime;
		});
	}

	protected final DocumentFile _documentFile;

	protected CachedItem(final DocumentFile documentFile) {
		this._documentFile = documentFile;
		initLazy();
	}

	public abstract boolean isDirectory();

	public Uri getUri() {
		return this._uri.get();
	}

	public String getName() {
		return this._name.get();
	}

	public String getMime() {
		return this._mime.get();
	}

	public boolean exists() {
		return this._documentFile.exists();
	}

	public long getLastModifiedUnix() {
		return this._documentFile.lastModified();
	}

	public Instant getLastModifiedInstant() {
		return Instant.ofEpochSecond(this._documentFile.lastModified());
	}

	public boolean rename(final String newName) {
		// Renaming could invalidate some fields.
		initLazy();
		return this._documentFile.renameTo(newName);
	}

	@Override
	public boolean equals(@Nullable Object obj) {
		if (obj instanceof CachedItem) {
			final CachedItem other = (CachedItem)obj;
			return this.getUri().equals(other.getUri()) && this.getMime().equals(other.getMime());
		}
		return false;
	}

	@Override
	public int hashCode() {
		final Uri uri = this.getUri();
		final String mime = this.getMime();
		return 7 * uri.hashCode() ^ 11 * mime.hashCode();
	}
}
