package org.codeberg.mek101.markitdown.ui.files;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import org.codeberg.mek101.markitdown.R;
import org.codeberg.mek101.markitdown.databinding.ActivityFileManagerBinding;
import org.codeberg.mek101.markitdown.ui.files.directoryview.DirectoryViewFragment;
import org.codeberg.mek101.markitdown.ui.utils.UtilsActivity;
import org.codeberg.mek101.markitdown.viewmodel.files.FileManagerViewModel;
import org.codeberg.mek101.markitdown.viewmodel.files.ViewModelProviderFactory;

import java.util.Optional;

public class FileManagerActivity extends AppCompatActivity {

	private static final String LOG_NAME = "FileManagerActivity";

	private ActivityFileManagerBinding _binding;
	private FileManagerViewModel _model;


	private void setContentFragment(final Optional<Uri> fileStorageRoot) {
		final FragmentManager fragmentManager = getSupportFragmentManager();
		if (fileStorageRoot.isPresent()) {
			Log.i(LOG_NAME, "Setting content fragment to " + fileStorageRoot);
			fragmentManager.beginTransaction()
					.replace(R.id.file_container, DirectoryViewFragment.newInstance(fileStorageRoot.get()))
					.setReorderingAllowed(true)
					.commit();
		} else {
			Log.w(LOG_NAME, "Couldn't access the file storage to " + fileStorageRoot);
			fragmentManager.beginTransaction()
					.replace(R.id.file_container, NoFileStorageFragment.newInstance())
					.commit();
		}
	}

	/**
	 * Opens the utils activity into to the given utility.
	 */
	private void goToUtilActivity(final int optionsId) {
		startActivity(new Intent(this, UtilsActivity.class).putExtra(UtilsActivity.UTILITY_KEY, optionsId));
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this._binding = ActivityFileManagerBinding.inflate(getLayoutInflater());

		setContentView(this._binding.getRoot());
		setSupportActionBar(this._binding.toolbar);

		this._model = ViewModelProviderFactory.getFileManagerViewModelProvider(this, getApplication()).get(FileManagerViewModel.class);
		this._model.getFileStorageUri().observe(this, uri -> {
			Log.i(LOG_NAME, "Reacting to observed file storage change");
			setContentFragment(uri);
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_file_manager, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		final int optionId = item.getItemId();

		if (optionId == R.id.option_settings) {
			goToUtilActivity(UtilsActivity.SETTINGS_UTILITY);
			return true;
		} else if (optionId == R.id.option_about) {
			goToUtilActivity(UtilsActivity.ABOUT_UTILITY);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onSupportNavigateUp() {
//		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_container);
//		return NavigationUI.navigateUp(navController, this._appBarConfiguration)
//				|| super.onSupportNavigateUp();
		return super.onSupportNavigateUp();
	}
}
