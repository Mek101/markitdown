package org.codeberg.mek101.markitdown.backend.files;


import android.net.Uri;
import android.os.Build;
import android.os.FileObserver;

import androidx.annotation.Nullable;

import java.io.File;
import java.util.function.BiConsumer;

/**
 * A wrapper for building file observers compatible with the system API level.
 */
class FileObserverFactory {
	/**
	 * Prevent instantiation.
	 */
	private FileObserverFactory() { }

	public static FileObserver buildFileObserver(final Uri folderUri, final int mask, final BiConsumer<Integer, String> callback) {
		if (Build.VERSION.SDK_INT < 29) {
			@SuppressWarnings("deprecation")
			FileObserver fileObserver = new FileObserver(folderUri.getPath()) {
				@Override
				public void onEvent(int event, @Nullable String path) {
					callback.accept(event, path);
				}
			};
			return fileObserver;
		} else {
			return new FileObserver(new File(folderUri.getPath())) {
				@Override
				public void onEvent(int event, @Nullable String path) {
					callback.accept(event, path);
				}
			};
		}
	}
}
