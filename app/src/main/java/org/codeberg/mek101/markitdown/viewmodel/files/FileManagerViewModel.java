package org.codeberg.mek101.markitdown.viewmodel.files;

import android.app.Application;
import android.content.UriPermission;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.codeberg.mek101.markitdown.backend.settings.SettingsStorage;
import org.codeberg.mek101.markitdown.utils.Utils;

import java.util.Optional;

/**
 * A view model for the file manager activity.
 */
public class FileManagerViewModel extends AndroidViewModel {
	private static final String LOG_NAME = "FileManagerViewModel";

	private final MutableLiveData<Optional<Uri>> _fileStorageUri;
	// Can have and expose it via a getter since Application is a global instance.
	private final SettingsStorage _settings;

	private boolean hasPermissions(final Uri directoryUri) {
		if (getApplication().getContentResolver().getPersistedUriPermissions().stream()
				.anyMatch(p -> p.getUri().equals(directoryUri))) {
			Log.i(LOG_NAME, "Has permissions from persisting");
			return true;
		}
		Log.i(LOG_NAME, "Trying to read uri");
		return Utils.isDocumentTreeAccessible(getApplication().getBaseContext(), directoryUri);
	}

	private void fileStorageCallback(final Optional<Uri> uri) {
		// Deduplicate duplicate calls
		if (!uri.equals(this._fileStorageUri.getValue())) {
			if (! uri.isPresent()) {
				this._settings.deleteFileStorage();
			}
			this._fileStorageUri.setValue(uri);
		}
	}

	@Override
	protected void onCleared() {
		this._settings.getFileStorageLive().removeObserver(this::fileStorageCallback);
	}

	public FileManagerViewModel(@NonNull Application application) {
		super(application);
		this._fileStorageUri = new MutableLiveData<>();

		this._settings = SettingsStorage.of(getApplication());

		final Optional<Uri> fileStorage = this._settings.getFileStorage();
		// If we don't have a file storage or we don't have permissions to access it, update the settings first.
		if (!fileStorage.isPresent() || !hasPermissions(fileStorage.get())) {
			this._settings.deleteFileStorage();
		}

		this._settings.getFileStorageLive().observeForever(this::fileStorageCallback);
		// Ensure there is a starting value.
		this._fileStorageUri.setValue(this._settings.getFileStorage());
	}

	public LiveData<Optional<Uri>> getFileStorageUri() {
		return this._fileStorageUri;
	}
}
