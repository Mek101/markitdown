package org.codeberg.mek101.markitdown.viewmodel.files;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;

import org.codeberg.mek101.markitdown.backend.files.cached.CachedDirectory;
import org.codeberg.mek101.markitdown.viewmodel.files.directoryview.DirectoryViewModel;

import java.util.function.Supplier;

/**
 * A factory for view model factories.
 */
public class ViewModelProviderFactory {

	private static ViewModelProvider.Factory createFactory(final Class<?> supportedClass, final Supplier<ViewModel> viewModelSupplier) {
		return new ViewModelProvider.Factory() {
			@NonNull
			@Override
			public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
				if (modelClass != supportedClass) {
					throw new IllegalArgumentException("Unsupported type '" + modelClass + "'");
				}
				@SuppressWarnings("unchecked")
				final T viewModel = (T) viewModelSupplier.get();
				return viewModel;
			}
		};
	}

	private static ViewModelProvider createProvider(final ViewModelStore store, final Class<?> supportedClass, final Supplier<ViewModel> viewModelSupplier) {
		return new ViewModelProvider(store, createFactory(supportedClass, viewModelSupplier));
	}

	/**
	 * @param owner       the viewmodel store owner.
	 * @param application the application singleton.
	 * @param directory   the directory to be viewed.
	 *
	 * @return a DirectoryViewModel factory.
	 */
	public static ViewModelProvider getDirectoryViewModelProvider(final ViewModelStoreOwner owner, final Application application, final CachedDirectory directory) {
		return getDirectoryViewModelProvider(owner.getViewModelStore(), application, directory);
	}

	/**
	 * @param store       the viewmodel store.
	 * @param application the application singleton.
	 * @param directory   the directory to be viewed.
	 *
	 * @return a DirectoryViewModel factory.
	 */
	public static ViewModelProvider getDirectoryViewModelProvider(final ViewModelStore store, final Application application, final CachedDirectory directory) {
		return createProvider(store, DirectoryViewModel.class, () -> new DirectoryViewModel(application, directory));
	}

	/**
	 * @param owner       the viewmodel store owner.
	 * @param application the application singleton.
	 *
	 * @return a FileManagerViewModel factory.
	 */
	public static ViewModelProvider getFileManagerViewModelProvider(final ViewModelStoreOwner owner, final Application application) {
		return getFileManagerViewModelProvider(owner.getViewModelStore(), application);
	}

	/**
	 * @param store       the viewmodel store.
	 * @param application the application singleton.
	 *
	 * @return a FileManagerViewModel factory.
	 */
	public static ViewModelProvider getFileManagerViewModelProvider(final ViewModelStore store, final Application application) {
		return createProvider(store, FileManagerViewModel.class, () -> new FileManagerViewModel(application));
	}
}
