package org.codeberg.mek101.markitdown.backend.files.cached;

import androidx.documentfile.provider.DocumentFile;

import java.util.Arrays;
import java.util.stream.Stream;

public class CachedDirectory extends CachedItem {
	public CachedDirectory(final DocumentFile documentFile) {
		super(documentFile);
	}

	public Stream<CachedItem> getContent() {
		return Arrays.stream(super._documentFile.listFiles()).map(CachedItem::fromDocumentFile);
	}

	@Override
	public boolean isDirectory() {
		return true;
	}
}
